#include "Timers.h"
#include "mcc_generated_files/tmr0.h"
#include "mcc_generated_files/tmr2.h"

bool timerDone(timer_t * t)
{    
    if(millis()>=(t->interval+t->prevTime) )
	{
		t->prevTime=millis();
		return true;
	}
	else
	{
		return false;
	}
}

void setTimerInterval(timer_t * t,unsigned int intervalSetting)
{
    t->interval=intervalSetting;
}

void resetTimer(timer_t * t)
{
    t->prevTime=millis();
}

bool timerDoneMicros(timer_micros_t * t)
{
    
    if(micros()>=(t->interval+t->prevTime) )
	{
		t->prevTime=micros();
		return true;
	}
	else
	{
		return false;
	}
}

void setTimerIntervalMicros(timer_micros_t * t,unsigned int intervalSetting)
{
    t->interval=intervalSetting;
}

void resetTimerMicros(timer_micros_t * t)
{
    t->prevTime=micros();
}
