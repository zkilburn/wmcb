/* 
 * File:   Timers.h
 * Author: Zac
 *
 * Created on December 12, 2017, 7:52 PM
 */

#ifndef TIMERS_H
#define	TIMERS_H

#include <stdbool.h>

typedef struct{
    unsigned long prevTime;
    unsigned long interval;
}timer_t;

typedef struct{
    unsigned long prevTime;
    unsigned long interval;
}timer_micros_t;

bool timerDone(timer_t * t);
void setTimerInterval(timer_t * t,unsigned int intervalSetting);
void resetTimer(timer_t * t);

bool timerDoneMicros(timer_micros_t * t);
void setTimerIntervalMicros(timer_micros_t * t,unsigned int intervalSetting);
void resetTimerMicros(timer_micros_t * t);



#endif	/* TIMERS_H */

