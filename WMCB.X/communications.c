#include "communications.h"
#include "Timers.h"
#include "mcc_generated_files/tmr2.h"
#include "mcc_generated_files/tmr3.h"
#include "mcc_generated_files/tmr0.h"
#include "mcc_generated_files/eusart.h"
#include "LED.h"
#include <xc.h>

#define DEADBAND    5

uint8_t speedSetting;   

int receiveArray[15];

void initCommunications(void)
{
    begin(receiveArray, sizeof(receiveArray),WMCB_ADDRESS, false, EUSART_Write, EUSART_Read, EUSART_Bytes_Available, EUSART_Peek); //ask zac for address location
}
void updateComms(void)
{   
       static bool lastSeen=false;
        if(getTransmitStall())
        {
            if(lastSeen==true)
            {
                if(TMR3_ReadTimer()>125)
                {
                    PORTAbits.RA2=1;                
                    lastSeen=false;
                }
            }
        }
        else if(getTransmitStall()==false)
        {
            lastSeen=true;
            TMR3_WriteTimer(0);
        }        
         
        if (receiveData())
        {
            toggleLED(3);
            
            //Send a response to the master
            ToSend(0,WMCB_ADDRESS);
            sendData(MCB_ADDRESS);           
            
            speedSetting=(uint8_t)receiveArray[SPEED_SETTING];        //Set New Speed
            if(speedSetting < DEADBAND)
            {
                speedSetting=0;
            }
            if (speedSetting != 0)              //Speed setting is non zero
            {
                toggleLED(1);
            }
            else                                //Speed setting 0
            {
                toggleLED(2);   
            }         
        } 
        
}

void setSpeedSetting(uint8_t speed)
{
    speedSetting=speed;
}
uint8_t getSpeedSetting()
{
    return speedSetting;
}
int * getReceiveArrayComms(void)
{
    return receiveArray;
}
