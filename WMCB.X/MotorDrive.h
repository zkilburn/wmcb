/* 
 * File:   MotorDrive.h
 * Author: Zac
 *
 * Created on December 12, 2017, 8:11 PM
 */

#ifndef MOTORDRIVE_H
#define	MOTORDRIVE_H

#include <stdbool.h>

void initMotorDrive(void);
void updateMotorDrive(void);
void testMotorControl(void);



#endif	/* MOTORDRIVE_H */

