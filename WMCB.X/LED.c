#include "LED.h"
#include "Timers.h"
timer_t blinky;
void initLEDs(void)
{
    setTimerInterval(&blinky,1000);
    
    LED1      = LED_OFF;
    LED2      = LED_OFF;
    LED3      = LED_OFF;
    LED4      = LED_OFF;
    
}

void setLED(unsigned int ledNum, unsigned int state)
{
   switch(ledNum)
   {
       case 1:
           LED1 = state;
           break;
       case 2:
           LED2 = state;
           break;
       case 3:
           LED3 = state;           
           break;
       case 4:
           LED4 = state;
           break;
   }
}

void toggleLED(unsigned int ledNum)
{
    switch(ledNum)
   {
       case 1:
           LED1 ^= 1;
           break;
       case 2:
           LED2 ^= 1;
           break;
       case 3:
           LED3 ^= 1;          
           break;
       case 4:
           LED4 ^= 1;
           break;
   }    
}
void blinkLED(unsigned int ledNum)
{
    if(timerDone(&blinky))
    {
        toggleLED(ledNum);
        resetTimer(&blinky);
    }
}