/* 
 * File:   LED.h
 * Author: Zac
 *
 * Created on December 12, 2017, 7:12 PM
 */

#ifndef LED_H
#define	LED_H

#include <xc.h>

#define OUTPUT              0
#define INPUT               1

#define LED_ON              1
#define LED_OFF             0

#define LED1_TRIS           TRISCbits.TRISC5
#define LED2_TRIS           TRISCbits.TRISC4
#define LED3_TRIS           TRISCbits.TRISC6
#define LED4_TRIS           TRISCbits.TRISC7

#define LED1                LATCbits.LATC5
#define LED2                LATCbits.LATC4
#define LED3                LATCbits.LATC6
#define LED4                LATCbits.LATC7

void initLEDs(void);
void setLED(unsigned int ledNum, unsigned int state);
void toggleLED(unsigned int ledNum);
void blinkLED(unsigned int ledNum);

#endif	/* LED_H */

