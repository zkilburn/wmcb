/* 
 * File:   communications.h
 * Author: Zac
 *
 * Created on December 12, 2017, 6:51 PM
 */

#ifndef COMMUNICATIONS_H
#define	COMMUNICATIONS_H
#include <stdint.h>
#include "FastTransfer.h"

#define SPEED_SETTING   1
#define MCB_ADDRESS     1
#define WMCB_ADDRESS    3

void initCommunications(void);
void updateComms(void);
int * getReceiveArrayComms(void);
uint8_t getSpeedSetting();
void setSpeedSetting(uint8_t speed);
    


#endif	/* COMMUNICATIONS_H */

