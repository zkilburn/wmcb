#include "MotorDrive.h"
#include "mcc_generated_files/pwm5.h"
#include "communications.h"
#include "Timers.h"
#include "LED.h"

bool motorDriveEnabled=false;
uint8_t previousSpeedSetting=0;
timer_t rampTimer, weaponMotorTimeout;

    
void initMotorDrive(void)
{
    setTimerInterval(&rampTimer,10);    
    setTimerInterval(&weaponMotorTimeout, 1000);    
    PWM5_LoadDutyValue(0);
}

void updateMotorDrive(void)
{
        if(timerDone(&rampTimer))
        {
            if(previousSpeedSetting < getSpeedSetting())             //If Previous speed is less than Get speed
            {
                previousSpeedSetting++;                     //Increase PWM
                PWM5_LoadDutyValue((uint16_t)previousSpeedSetting*10);   //Set PWM based on speed setting
            }
            else if (previousSpeedSetting > getSpeedSetting())       //If Previous speed is more than Get speed
            {
                previousSpeedSetting--;                     //Decrease PWM
                PWM5_LoadDutyValue((uint16_t)previousSpeedSetting*10);   //Set PWM based on speed setting
            }
            else                                            //If Previous speed is same as Get speed
            {
                
            }

            if(previousSpeedSetting == 0 && getSpeedSetting() == 0)   //If Speed setting is 0
            {
                PWM5_LoadDutyValue(0);
            }
        }   
        
        if(timerDone(&weaponMotorTimeout))
        {
            setSpeedSetting(0);
        }
}

timer_t pwmTimer;
void testMotorControl(void)
{
    setTimerInterval(&pwmTimer,5);
    uint16_t i=0;
    while(1)
    {        
            //Speed up spin of motor
            for(i = 0; i <1023;i++)
            {
                PWM5_LoadDutyValue(i);
                while(!timerDone(&pwmTimer))
                {                
                    blinkLED(4); //Board is Alive
                }
            }

            //Slow down speed of motor
            for(i=1023;i>0;i--)
            {
                PWM5_LoadDutyValue(i);
                while(!timerDone(&pwmTimer))
                {                
                    blinkLED(4); //Board is Alive
                }
            }
    }
}