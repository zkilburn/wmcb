#define SPEED_SETTING 1

#include "communications.h"
#include "FastTransfer.h"
#include "LED.h"
#include "MotorDrive.h"
#include "Timers.h"

#include <stdio.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"

void main(void)
{
    
    // initialize the device
    SYSTEM_Initialize();

    initLEDs();
    setLED(1,1);
    initMotorDrive();                                           //Turn on Motor
    initCommunications();                                       //Start Comms

    TRISAbits.TRISA2 = 0;
    LATAbits.LATA2=0;
    //testMotorControl();
    while(1)
    {        
        blinkLED(4); //Board is Alive
        
        updateMotorDrive();
        
        updateComms();

    }
}

            
